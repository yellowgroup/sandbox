provider "google" {
    project = var.project_id
    region  = var.project_region
}

module "cloud-run" {
    source      = "../../modules/cloud-run"

    regions     = var.regions
    image       = var.image
    project_id  = var.project_id
    branch      = var.branch
}