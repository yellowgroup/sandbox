terraform {
    backend "gcs" {
        bucket = var.bucket
        prefix = "env/${var.branch}"
    }
}