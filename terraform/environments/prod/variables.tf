variable "regions" {
    type        = map
    description = "regions to deploy cloud run into"
}

variable "image" {
    type        = string
    description = "docker image to be attached into cloud run instances"

}

variable "project_id" {
    type        = string
    description = "GCP Project ID"
}

variable "project_region" {
    type        = string
    description = "default region for actual project"
}

variable "branch" {
    type        = string
    description = "repo branch"
}

# backend
variable "bucket" {
    type        = string
    description = "GCS bucket for storing Terraform state"
}