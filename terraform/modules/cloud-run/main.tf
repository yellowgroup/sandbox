# resource "google_cloud_run_service" "default" {

#     name = "us-cloud-run"
#     location = "us-central1"
#     project = "ci-cd-sandbox"

#     template {
#         spec {
#             containers {
#                 image = "gcr.io/ci-cd-sandbox/bitbucket.org/yellowgroup/sandbox:master"
#             }
#         }
#     }
# }
module "cloud_run_service" {
  source   = "garbetjie/cloud-run/google"
  version  = "~.>"

  for_each = var.regions

  # Required parameters
  name     = "cr-${each.value}-${var.branch}"
  image    = var.image
  location = each.value

  # Optional parameters
  allow_public_access = true
  ingress             = "internal-and-cloud-load-balancing"
  port                = 3000
  project             = var.project_id

}
resource "google_compute_region_network_endpoint_group" "default" {
  for_each              = var.regions

  name                  = "cr-${each.key}-neg-${var.branch}"
  network_endpoint_type = "SERVERLESS"
  region                = module.cloud_run_service[each.key].location
  cloud_run {
    service = module.cloud_run_service[each.key].name
  }
}
module "lb-http" {
  source            = "GoogleCloudPlatform/lb-http/google//modules/serverless_negs"
  version           = "~> 4.5"

  project           = var.project_id
  name              = "cr-lb-http-${var.branch}"

  ssl                             = false
  managed_ssl_certificate_domains = []
  https_redirect                  = false
  backends = {
    default = {
      description            = null
      enable_cdn             = false
      custom_request_headers = null

      log_config = {
        enable      = true
        sample_rate = 1.0
      }

      groups = [

        for neg in google_compute_region_network_endpoint_group.default:
        {
          group = neg.id
        }

      ]

      iap_config = {
        enable               = false
        oauth2_client_id     = null
        oauth2_client_secret = null
      }
      security_policy = null
    }
  }
}

