variable "regions" {
    type        = map
    description = "regions to deploy cloud run into"
}

variable "image" {
    type        = string
    description = "docker image to be attached into cloud run instances"

}

variable "project_id" {
    type        = string
    description = "GCP Project ID"
}

variable "branch" {
    type        = string
    description = "repo branch"
}